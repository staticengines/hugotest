+++
title = "Static Element #3"
+++


Floating UL Elements

<h2 class="noauth"><!-- InstanceBeginEditable name="page_title" -->IPv6 Info Center<!-- InstanceEndEditable --></h2>
  <div id="maincontent">
  
<!-- InstanceBeginEditable name="Content" -->
<div class="highlights" style="float:right;">
       <h4>On this page&hellip;</h4>
       <ul>
<li><a href="#ipv6">What is IPv6?</a></li>
         <li><a href="#need">Why Do You Need IPv6? </a></li>
         <li><a href="#preparing">Preparing for IPv6</a></li>
         <li><a href="#how">How Do I Get IPv6?</a></li>
<li><a href="#next">What Do I Do Next?</a></li>
         <li><a href="#ipv4">How is IPv6 Different than IPv4? </a></li>
        </ul>
     </div>
<p><strong>This page contains information on how your organization can prepare to adopt IPv6. </strong></p>
<h3 id="ipv6">What is IPv6?</h3>
<p>Internet Protocol version 6 (IPv6) is the latest IP revision, developed as a successor to IPv4. IPv6 provides a much larger address pool so that many more devices can be connected to the Internet. It also improves addressing and routing of network traffic. Because the free pool of IPv4 addresses has been depleted, customers will want to request IPv6 address space for new networks, and eventually transition their networks from IPv4 to IPv6.</p>
<p>For a more complete understanding of IPv6,  the video below provides a walkthrough of many of the finer details of IPv6.</p>
<iframe width="600" height="338" src="https://www.youtube.com/embed/rWJZfShWE6g" frameborder="0" allowfullscreen></iframe>

<h3 id="need">Why Do You Need IPv6?</h3>
<p>Now that IPv4 is depleted, there are extra costs associated with staying IPv4-only, which will likely increase over time. On the other hand, it is easy to get IPv6 from ARIN, there are generally no additional costs for ISPs, and fees were recently reduced for end users. </p>
<p>Don&rsquo;t forget that we also have a reserve IPv4 block that is dedicated for IPv6 transition support, which you can read more about in ARIN's <a href="/policy/nrpm.html#four10">Number Resource Policy Manual (Section 4.10)</a>. You can receive one /24 every six months to support your IPv6 transition effort.</p>

<h3 id="preparing">Preparing for IPv6</h3>
<p>Before you implement IPv6, it's a good idea to make sure your equipment, software, and staff are ready. Think about how many network addresses you'll need, and how you'll set up your network. </p>
<p>To learn more about how to determine how much IPv6 address space you need, visit the <a href="/resources/ipv6_planning.html">IPv6 Planning page</a>.</p>
<p>Get advice from those who have already adopted IPv6, and ask questions! You can visit the <a href="http://getipv6.info">IPv6 wiki</a> to help you research your options. You can also visit the Internet Society&rsquo;s <a href="http://www.internetsociety.org/deploy360/">Deploy360 page</a> to find detailed IPv6 deployment case studies from ISPs, hosting providers, enterprise businesses, universities, and governments.</p>