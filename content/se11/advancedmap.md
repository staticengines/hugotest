+++
title = "Static Element #11"
+++

This is the Advanced Interactive RIR Map for the 5 Registries (nicer version) -- this a test with GitEye
This is also a test using Dreamweaver with Git integrated

<p>Regional Internet Registries (RIRs) are  nonprofit corporations that administer and register Internet Protocol (IP)  address space and Autonomous System (AS) numbers within a <a href="https://www.nro.net/about-the-nro/list-of-country-codes-and-rirs-ordered-by-country-code" class="external">defined  region</a>. RIRs also work together on joint projects. </p>

Here goes the interactive map:

{{< se11 >}}