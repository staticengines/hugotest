+++
title = "Static Element #9"
+++

Code Sample

{{< highlight html >}}
<h5>Example Payload</h5>
<pre><code>
&lt;poc xmlns=&quot;http://www.arin.net/regrws/core/v1&quot; &gt;
    &lt;iso3166-2&gt;VA&lt;/iso3166-2&gt;
    &lt;iso3166-1&gt;
        &lt;name&gt;UNITED STATES&lt;/name&gt;
        &lt;code2&gt;US&lt;/code2&gt;
        &lt;code3&gt;USA&lt;/code3&gt;
        &lt;e164&gt;1&lt;/e164&gt;
    &lt;/iso3166-1&gt;
</code></pre>
{{< /highlight >}}
